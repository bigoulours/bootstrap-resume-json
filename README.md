# bootstrap-resume-json

Forked from [startbootstrap-resume](https://github.com/startbootstrap/startbootstrap-resume) and adapted to work with [json-resume](https://github.com/jsonresume/resume-schema).
This resume is available as [Gitlab Page](https://bigoulours.gitlab.io/resume/).

![screenshot](screenshot.png)

## Installation

Install resume-cli globally if you haven't already:
```
sudo npm install -g resume-cli
```
Clone this repository to the same folder where your resume.json file is located (otherwise add it as a submodule of your project):
```
git clone git@gitlab.com:bigoulours/bootstrap-resume-json.git
```
Install as jsonresume-theme (the theme is then called "online"):
```
npm install bootstrap-resume-json
```
Generate your resume with:
```
resume export bootstrap-resume-json/public/index.html -t online
```
The `public` folder is now ready to be served on your server.
